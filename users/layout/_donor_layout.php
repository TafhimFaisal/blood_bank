<div class="container">
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10">
            <?php if(isset($donors)): $i++;?>
            <table class="table table-condensed">
                <thead>
                <th>id</th>
                <th>Name</th>
                <th>Sex</th>
                <th>Bload Type</th>
                <th>Date of Birth</th>
                <th>City</th>
                <th>Mobile</th>
                </thead>
                <?php foreach($donors as $u): $i++;?>
                <tr class="<?php if($i%2==0){echo $bg_background;}else{echo 'bg-danger';} ?>">
                    <td><?= $u['id']; ?></td>
                    <td><?= $u['fname']." ".$u['lname']; ?></td>
                    <td><?= $u['sex']; ?></td>
                    <td><?= $u['b_type']; ?></td>
                    <td><?= $u['bday']; ?></td>
                    <td><?= $u['city']; ?></td>
                    <td><?= $u['mobile'];?></td>
                </tr>
                <?php endforeach; ?>
            </table>
            <?php endif; ?>
        </div>
        <div class="col-md-1"></div>
    </div>
</div>