-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 22, 2018 at 03:29 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `donor`
--

-- --------------------------------------------------------

--
-- Table structure for table `donors`
--

CREATE TABLE `donors` (
  `id` int(11) NOT NULL,
  `fname` varchar(45) NOT NULL,
  `mname` varchar(30) DEFAULT NULL,
  `lname` varchar(45) NOT NULL,
  `sex` varchar(10) NOT NULL,
  `b_type` varchar(2) NOT NULL,
  `bday` date NOT NULL,
  `h_address` varchar(50) NOT NULL,
  `city` varchar(30) NOT NULL,
  `don_date` date NOT NULL,
  `stats` text NOT NULL,
  `temp` varchar(10) NOT NULL,
  `pulse` varchar(10) NOT NULL,
  `bp` varchar(10) NOT NULL,
  `weight` int(11) NOT NULL,
  `hemoglobin` varchar(25) NOT NULL,
  `hbsag` varchar(10) NOT NULL,
  `aids` varchar(15) NOT NULL,
  `malaria_smear` varchar(20) NOT NULL,
  `hematocrit` varchar(15) NOT NULL,
  `phone` varchar(10) DEFAULT NULL,
  `mobile` varchar(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `donors`
--

INSERT INTO `donors` (`id`, `fname`, `mname`, `lname`, `sex`, `b_type`, `bday`, `h_address`, `city`, `don_date`, `stats`, `temp`, `pulse`, `bp`, `weight`, `hemoglobin`, `hbsag`, `aids`, `malaria_smear`, `hematocrit`, `phone`, `mobile`) VALUES
(30, 'Mahamud', '', 'Fahad', 'male', 'O+', '1994-07-16', 'Chittagong', 'Chittagong', '2015-04-18', 'Normal', '30', '60', '80 | 120', 64, '16 - 18 gm/dl', 'Negative', 'Negative', 'Negative', '45 - 62%', '2632181', '9827983762'),
(33, 'Sheikh', 'Riyad', 'Rocky', 'male', 'A-', '1995-02-08', 'dhaka', 'dhaka', '2018-03-13', '111', '98', '80', '100', 90, '60', '123', 'no', 'no', 'no', '', '981231989'),
(31, 'dash', '', 'Shudipta', 'female', 'A+', '1994-02-02', 'Davpahar,chittagong', 'Jamalkhan', '2015-04-18', 'Normal', '30', '60', '80 | 120', 64, '16 - 18 gm/dl', 'Negative', 'Negative', 'Negative', '45 - 62%', '', '8602042302'),
(32, 'MD', '', 'Sazzad', 'male', 'O+', '1994-09-24', 'Khulshi', 'Khulshi', '2015-04-19', 'Normal', '30', '70', '80 | 120', 52, '16 - 18 gm/dl', 'Negative', 'Negative', 'Negative', '45 - 62%', '', '8269036096'),
(34, 'Sudipto', 'Dey', 'Jitu', 'male', 'B+', '1996-12-12', 'D.C Road, Chandanpura,Chittagong', 'chittagong', '2018-03-06', '111', '98', '80', '80', 60, '23', '23', 'no', 'no', 'no', '', '1834746298'),
(35, 'MD', 'Riyad', 'Sarkar', 'male', 'A+', '1990-12-07', 'Chittagong', 'chittagong', '1990-03-16', '111', '98', '96', '100', 90, '23', '123', 'no', 'no', 'no', '19123453', '019123456'),
(46, 'Sheikh', 'Riyad', 'SHiraj', 'male', 'O-', '2018-03-02', 'eFASfASFas', 'chittagong', '2018-03-15', '1111', '98', '12', '100', 90, '23', '123', 'no', 'no', 'no', '1342412435', '1235153623'),
(45, 'MD', 'Riyad', 'Rocky', 'male', 'O+', '2018-03-14', 'dfaSA', 'chittagong', '2018-03-22', '1111', '98', '123', '100', 90, '60', '123', 'no', 'no', 'no', '123453', '123445');

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` int(11) NOT NULL,
  `f_name` varchar(35) NOT NULL,
  `m_name` varchar(15) DEFAULT NULL,
  `l_name` varchar(35) NOT NULL,
  `username` varchar(15) NOT NULL,
  `password` varchar(15) NOT NULL,
  `b_day` date NOT NULL,
  `prc_nr` int(25) NOT NULL,
  `designation` varchar(35) NOT NULL,
  `landline` varchar(10) DEFAULT NULL,
  `mobile_nr` varchar(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `f_name`, `m_name`, `l_name`, `username`, `password`, `b_day`, `prc_nr`, `designation`, `landline`, `mobile_nr`) VALUES
(11, 'Tafhim', '', 'MD', 'tafhim', '123', '1994-07-16', 2147483647, 'Student', '2632181', '9827983762'),
(18, 'Sheikh', NULL, 'SHiraj', 'Sheikh', '1234', '2018-03-14', 0, 'student', '122234', '01898966'),
(19, 'raba', 'noor', 'khan', 'raba', '123', '2018-03-27', 1111112, 'student', '1111111111', '11111111111'),
(20, 'MD', 'noor', 'Rocky', 'noor', '222', '2018-03-27', 222333, 'student', '12324', '12345'),
(21, 'MD', 'noor', 'Rocky', 'noor12', '12345', '2018-03-26', 123117, 'student', '2222222222', '1111111111'),
(22, 'tata', 'bla', 'bla', 'tata', '123456', '2018-03-27', 1019283, 'student', '3333333333', '11111111111'),
(23, 'Barat', 'Rojas', 'Ahammed', 'barat23', '123456', '1995-01-16', 1706351202, 'Student', '0170635120', '01706351202');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `dob` date NOT NULL,
  `gender` varchar(10) NOT NULL,
  `b_type` varchar(10) NOT NULL,
  `address` varchar(500) NOT NULL,
  `city` varchar(100) NOT NULL,
  `mobile` varchar(13) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `dob`, `gender`, `b_type`, `address`, `city`, `mobile`, `created_at`) VALUES
(1, 'Tafhim', 'Faisal', 'tafhim@gmail.com', '1994-07-16', 'Male', 'O+', 'Chittagong,davpahar', 'davpahar', '018123654', '2018-03-16 10:12:35'),
(2, 'Sazzad', 'Hussain', 'sazzadhusai@gmail.com', '1994-09-24', 'male', 'B+', 'Chittagong,chowkbazar\r\n', 'chowkbazar', '01987234', '2018-03-16 10:10:23'),
(3, 'Mahamud', 'Fahad', 'fahad@gmail.com', '2000-02-15', 'Male', 'O+', 'Muradpur,bahoddarhat', 'Jabalpur', '8871479418', '2018-03-16 10:13:45'),
(4, 'Tafhim', 'Faisal', 'tafhimfaisal@gmail.com', '1995-12-12', 'Male', 'B+', 'Bangladesh,chittagong', 'chittagong', '01822938426', '2018-03-15 20:03:57'),
(17, 'raba', 'khan', 'raba@gmail.com', '2018-03-27', 'Male', 'A+', 'adsadasdads', 'davpahar', '11111111111', '2018-03-22 04:25:30'),
(18, 'MD', 'Rocky', 'noor@gmail.com', '2018-03-27', 'Male', 'A+', 'asssssdsdsff', 'dhaka', '12345', '2018-03-22 04:27:13'),
(19, 'MD', 'Rocky', '123sa@gmail.cm', '2018-03-26', 'Female', 'A+', 'asdfghjkb', 'chittagong', '1111111111', '2018-03-22 07:30:03'),
(20, 'tata', 'bla', 'tata@gmail.com', '2018-03-27', 'Female', 'A+', 'kaksjhdhfhueue', 'dhaka', '1111111111111', '2018-03-22 07:51:19'),
(21, 'Barat', 'Ahammed', 'baratahmed28@gmail.com', '1995-01-16', 'Male', 'AB+', '1 no police gate, Dampara, WASA, CTG.', 'chittagong', '01706351202', '2018-03-22 14:10:40'),
(22, 'Javed', 'Talukder', 'xxaved@gmail.com', '1995-04-06', 'Male', 'AB+', '2 no gate, chittagong', 'chittagong', '932038', '2018-03-22 14:14:20');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `donors`
--
ALTER TABLE `donors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `prc_nr` (`prc_nr`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `donors`
--
ALTER TABLE `donors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
